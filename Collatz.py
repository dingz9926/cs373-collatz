#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List

# ------------
# collatz_read
# ------------

global_dict = dict()
global_dict[1] = 1

# given
def path_length(n) -> int:
    # we can safely assert since it is an internal function
    assert n > 0
    c = 1
    while n > 1:
        # search in global cache
        temp = global_dict.get(n)
        if temp is not None:
            return c + temp - 1
        if (n % 2) == 0:
            n = n // 2
        else:
            # n = (3 * n) + 1
            # next step is divide 2, so in two steps we did: n = 1.5n+0.5
            n = int(1.5 * n + 1)
            # two steps so add c again inside
            c += 1
        c += 1
    assert c > 0
    assert c is not None
    return c


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # <your code>
    # global_dict is actually a local dict here
    # assert i and j value according to the definition
    assert 0 < i < 1000000
    assert 0 < j < 1000000
    left, right = min(i, j), max(i, j)
    # after finding the correct bound, checking using assertion
    assert left <= right
    max_len = -1
    left_bound = left
    # optimization according to the quiz
    # if we can find a counter part of the left half by multiplying too,
    # then we can change the left bound
    temp_left = right // 2 + 1
    if temp_left > left:
        left_bound = temp_left
    #     only need to find max even number
    for my_num in range(left_bound, right + 1):
        # search in global cache
        temp = global_dict.get(my_num)
        if temp is not None:
            my_solution = temp
        else:
            my_solution = path_length(my_num)
            global_dict[my_num] = my_solution
        assert isinstance(my_solution, int)
        assert isinstance(max_len, int)
        assert my_solution is not None
        assert max_len is not None
        if my_solution > max_len:
            max_len = my_solution
    return max_len


# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    # since path length remains constant for numbers, we might as well
    # have a global cache that can be used by different test cases
    # just found out collatz_solve is not necessarily called before collatz_eval
    for s in r:
        i, j = collatz_read(s)
        # i is not guaranteeed to be less than j so we need to take care of that
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
